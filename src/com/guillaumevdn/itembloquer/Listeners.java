package com.guillaumevdn.itembloquer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import com.guillaumevdn.itembloquer.util.ConfigItem;

public class Listeners implements Listener {

	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
	public void event(PlayerDropItemEvent event) {
		ConfigItem item = Main.inst().getConfigItem(event.getItemDrop().getItemStack());
		if (item != null) {
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
	public void event(PlayerPickupItemEvent event) {
		ConfigItem item = Main.inst().getConfigItem(event.getItem().getItemStack());
		if (item != null) {
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
	public void event(ItemSpawnEvent event) {
		ConfigItem item = Main.inst().getConfigItem(event.getEntity().getItemStack());
		if (item != null) {
			event.setCancelled(true);
		}
	}

}
