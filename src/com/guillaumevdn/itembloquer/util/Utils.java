package com.guillaumevdn.itembloquer.util;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Utils {

	public static boolean isUnbreakable(ItemStack item) {
		ItemMeta meta = item.getItemMeta();
		if (meta == null) return false;
		try {
			return (boolean) ItemMeta.class.getDeclaredMethod("isUnbreakable").invoke(meta);
		} catch (Throwable ignored) {
			try {
				Object spigot = ItemMeta.class.getDeclaredMethod("spigot").invoke(meta);
				return (boolean) spigot.getClass().getDeclaredMethod("isUnbreakable").invoke(spigot);
			} catch (Throwable ignored2) {
			}
		}
		return false;
	}

}
