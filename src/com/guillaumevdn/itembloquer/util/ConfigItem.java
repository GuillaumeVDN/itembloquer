package com.guillaumevdn.itembloquer.util;

import org.bukkit.Material;

public class ConfigItem {

	// base
	private Material type;
	private int durability;
	private boolean unbreakable;

	public ConfigItem(Material type, int durability, boolean unbreakable) {
		this.type = type;
		this.durability = durability;
		this.unbreakable = unbreakable;
	}

	// get
	public Material getType() {
		return type;
	}

	public int getDurability() {
		return durability;
	}

	public boolean getUnbreakable() {
		return unbreakable;
	}

}
